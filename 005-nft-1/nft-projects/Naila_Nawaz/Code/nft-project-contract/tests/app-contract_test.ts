
import { Clarinet, Tx, Chain, Account, types } from 'https://deno.land/x/clarinet@v0.14.0/index.ts';
import { assertEquals } from 'https://deno.land/std@0.90.0/testing/asserts.ts';

Clarinet.test({
    name: "Ensure that Tera coin can be minted",
    async fn(chain: Chain, accounts: Map<string, Account>) {
        const deployer = accounts.get("deployer")!;
        const tokenBuyer = accounts.get("wallet_1")!;

        let block = chain.mineBlock([
            Tx.contractCall("app-contract", 
            "mintNFT", 
            [types.principal(tokenBuyer.address), types.uint(100)],
             deployer.address
            ), 
        ]);

        block.receipts[0].result.expectOk().expectUint(1);
        assertEquals(block.receipts.length, 1);
        assertEquals(block.height, 2);
    },
});

Clarinet.test({
    name: "Ensure that Tera coin status can be changes to sell",
    async fn(chain: Chain, accounts: Map<string, Account>) {
        const deployer = accounts.get("deployer")!;
        const tokenBuyer = accounts.get("wallet_1")!;

        let block = chain.mineBlock([
            Tx.contractCall("app-contract", 
                "mintNFT", 
                [types.principal(tokenBuyer.address), types.uint(100)],
                deployer.address
            ), 
            Tx.contractCall("app-contract", 
                "sellNFT", 
                [types.uint(1)],
                tokenBuyer.address
            ), 
        ]);

        block.receipts[0].result.expectOk().expectUint(1);
        block.receipts[1].result.expectOk().expectBool(true);
        assertEquals(block.receipts.length, 2);
        assertEquals(block.height, 2);
    },
});

Clarinet.test({
    name: "Ensure that Tera coin status can be changes to sell and bid for NFT can be placed",
    async fn(chain: Chain, accounts: Map<string, Account>) {
        const deployer = accounts.get("deployer")!;
        const tokenBuyer = accounts.get("wallet_2")!;
        
        let block = chain.mineBlock([
            Tx.contractCall("app-contract", 
                "mintNFT", 
                [types.principal(tokenBuyer.address), types.uint(100)],
                deployer.address
            ), 
            Tx.contractCall("app-contract", 
                "sellNFT", 
                [types.uint(1)],
                tokenBuyer.address
            ), 
            Tx.contractCall("app-contract",
                "bidForNFT",
                [types.uint(1)],
                tokenBuyer.address
            ),
        ]);

        block.receipts[0].result.expectOk().expectUint(1);
        block.receipts[1].result.expectOk().expectBool(true);
        block.receipts[2].result.expectOk().expectBool(true);
        assertEquals(block.receipts.length, 3);
        assertEquals(block.height, 2);
    },
});


Clarinet.test({
    name: "Ensure that seller can respond to bid",
    async fn(chain: Chain, accounts: Map<string, Account>) {
        const deployer = accounts.get("deployer")!;
        const tokenBuyer = accounts.get("wallet_2")!;
        const contractOwner = accounts.get("wallet_1")!;
        const amount = 5;

        let block = chain.mineBlock([
            Tx.contractCall("app-contract", 
                "mintNFT", 
                [types.principal(tokenBuyer.address), types.uint(100)],
                deployer.address
            ), 
            Tx.contractCall("app-contract", 
                "sellNFT", 
                [types.uint(1)],
                tokenBuyer.address
            ), 
            Tx.contractCall("app-contract",
                "bidForNFT",
                [types.uint(1)],
                tokenBuyer.address
            ),
            Tx.contractCall("app-contract",
                "respondToBid",
                [types.bool(false), types.uint(1)],
                tokenBuyer.address
            ),
            Tx.contractCall("app-contract",
            "respondToBid",
            [types.bool(true), types.uint(1)],
            tokenBuyer.address
        ),
        ]);
        console.log(block.receipts);
        block.receipts[3].result.expectOk().expectBool(false);
        block.receipts[4].result.expectOk().expectBool(true);
    },
});