(impl-trait .nft-trait.nft-trait)

(define-non-fungible-token tera_coin uint)
;; my test net address
;; (define-constant CONTRACT_OWNER 'SP1H4SF8Z6KA2KZGD2W3VAJH6STG4XDAK4GWS5D67)
;; my local envirnoment address
(define-constant CONTRACT_OWNER 'ST1SJ3DTE5DN7X54YDH5D64R3BCB6A2AG2ZQ8YPD5)
(define-constant ERR_TOKEN_OWNER (err u101))
(define-constant ERR_NO_SELL (err u102))
(define-constant ERR_NO_RESPONSE_NEEDED (err u103))
(define-constant ERR_SELLER_RESPONSE (err u104))
(define-constant ERR_WAIT (err u105))
(define-constant ERR_NO_OWNER (err u106))
(define-constant ERR_BIDDER (err u107))
(define-data-var last-minted-token uint u0)
;; this will keep track of the cost of every tera coin
(define-map token_cost_mapping uint uint)
;; this will keep the track if a particular token is open for selling or not
;; uint is the token-id and bool is selling status (i.e. if NFT is up for sale or not)
(define-map token_status_mapping uint bool)
;; this will keep a map of the person who has bid for the coin. Only one person can bid
;; at one time. If the seller rejects the bid then buyer can bid with some other cost or some other person can bid
;; uint is the token-id and principal is token-buyer
(define-map token_buyer_mapping uint principal)
;; this will keep a track of bid, if it's accepted by seller or not 
;; uint is token-id and bool is bid status true bid status means seller has responded to bid
(define-map bid-mapping uint bool)
;;this will keep a track at what block height seller has accepted the bid
(define-map token_blockheight_mapping uint uint)

;; seller when wish to sell his property will create nft for that
(define-public (mintNFT (cost uint))
    (begin
        ;; the cost of token should be greater than 20 as per contract rules
        (asserts! (>= cost u20) (err u1))
        ( let
            (
                ;; create new token id with +1 in previous token
                (new-token-id (+ (var-get last-minted-token) u1))
            )
            (print new-token-id)
            ;; mint token for the receipent
            (try! (nft-mint? tera_coin new-token-id tx-sender))
            ;; save token, cost of token and receipent
            (map-insert token_cost_mapping new-token-id cost)
            (map-insert token_status_mapping new-token-id false)
            (var-set last-minted-token new-token-id)
            (ok new-token-id)
        )
    ) 
)

;; seller will publish it for selling by changing its selling status to true
(define-public (sellNFT (token-id uint))
    (let 
        (
            (owner (unwrap-panic (get-owner token-id)))
        )
        (asserts! (is-eq owner (some tx-sender)) ERR_TOKEN_OWNER )
        (map-set token_status_mapping token-id true)
        (ok true)
    )
)

;; buyer will bid for NFT if he is interested
(define-public (bidForNFT (token-id uint))
    (let
        (
            (sellingStatus (default-to false (map-get? token_status_mapping token-id)))
            (bidder (map-get? token_buyer_mapping token-id))
        )
        ;; Below line will check if the seller is selling this nft or not
        (asserts! sellingStatus ERR_NO_SELL)
        ;; and if there is already no buyer bidding for this nft then procede
        (if (is-none bidder)
            (begin
                ;; this will create a token-buyer mapping 
                (map-set token_buyer_mapping token-id tx-sender)
                ;; the following line will set the bidding status against this token to false, as seller hasn't accepted the bid yet
                (map-set bid-mapping token-id false)
                (ok true)
            )
            (begin
                (print "There's already a bidder bidding for this NFT")
                (ok false)
            )
        )
    )
)
;; seller will call this function to respond to a specific bid as accepted (true status) or rejected (false status),
;; if he accepts, he'll also agree to transfer 5% of deal price to the contract for contract owner
(define-public (respondToBid (status bool) (token-id uint))
    (let
        (
            ;; this will return true if the NFT is up for sale, in that case we can procede
            (sellingStatus (default-to false (map-get? token_status_mapping token-id)))
            ;; this will return false if seller has not responded to the bid, in that case we can procede
            ;; default value is set to true i.e  the seller has responded to bid and can't procede to respond again
            (sellerBidStatus (default-to true (map-get? bid-mapping token-id)))
        )
        
        (asserts! (and sellingStatus (not sellerBidStatus)) (ok false)) ;;not selling or already responded to bid
        (if status
            (let 
                (
                    (cost (default-to u1 (map-get? token_cost_mapping token-id)))
                    (dealProfit (/ (* cost u5) u100))
                )
                (map-set bid-mapping token-id true)
                (map-set token_blockheight_mapping token-id block-height)
                (print (unwrap-panic (map-get? token_blockheight_mapping token-id)))
                (try! (stx-transfer? dealProfit tx-sender (as-contract tx-sender)))
                (ok true)
            )
            (begin
                (print "REJECTED")
                (map-delete token_buyer_mapping token-id)
                (ok false)
            )
        )
    )
)
;;seller will call this function if the buyer has not submitted the deal price and he can claim his funds back
;; funds will be sent back after 20 blocks only
(define-public (claimFundsReversal (token-id uint))
    (let
        (
            ;; default value is set to false i.e  the seller has not responded to bid 
            (sellerBidStatus (default-to false (map-get? bid-mapping token-id)))
            (cost (default-to u1 (map-get? token_cost_mapping token-id)))
            (dealProfit (/ (* cost u5) u100))
            (dealBlockHeight (default-to u1 (map-get? token_blockheight_mapping token-id)))
            (owner (unwrap-panic (get-owner token-id)))
        )
        ;;this can be called only by the token owner 
        (asserts! (is-eq owner (some tx-sender)) ERR_TOKEN_OWNER )
        ;; if seller has responded to the bid then we can procede furthere else error will be thrown
        (asserts! sellerBidStatus ERR_NO_SELL)
        (asserts! (< (- block-height dealBlockHeight) u20) ERR_WAIT)
        (try! (stx-transfer? dealProfit (as-contract tx-sender) tx-sender))
        (map-delete token_buyer_mapping token-id)
        (map-delete bid-mapping token-id)
        (map-delete token_blockheight_mapping token-id)
        (ok true)
    )
)

;; buyer will call this function if he doesn't want to procede with the bid, after seller's acceptance
;; in this case, seller won't have to wait for his response
(define-public (backOffFromBid (token-id uint))
    (let
        (
            ;; default value is set to false i.e  the seller has not responded to bid 
            (sellerBidStatus (default-to false (map-get? bid-mapping token-id)))  
            (bidder (map-get? token_buyer_mapping token-id)) 
        )
        ;; this should be called by the bidder only
        (asserts! (is-eq bidder (some tx-sender)) ERR_BIDDER )
         ;; if seller has responded to the bid then we can procede furthere else error will be thrown
        (asserts! sellerBidStatus ERR_NO_SELL)
        (map-delete token_buyer_mapping token-id)
        (map-delete bid-mapping token-id)
        (map-delete token_blockheight_mapping token-id)
        (ok true)
    )
)
;; buyer will call this function, once seller responds accepted to the deal, and will transfer deal price for seller + 5% of 
;; of deal price for contract owner
(define-public (submitDealPrice (token-id uint))
    (let
        (
            ;; this will return true if seller has responded to the bit, in that case we can procede
            (sellerBidStatus (default-to false (map-get? bid-mapping token-id)))
            ;; deal price that seller has to give to contract owner i.e. 5%
            (cost (default-to u1 (map-get? token_cost_mapping token-id)))
            (dealProfit (/ (* cost u5) u100))
            (costOnSeller (+ cost dealProfit))
            (owner (unwrap-panic (get-owner token-id)))
        )
        ;; if the NFT is up for sale and seller has responded to the bid
        (asserts! sellerBidStatus (ok false))
        (try! (stx-transfer? costOnSeller tx-sender (as-contract tx-sender)))
              
        ;; this part is auto-execution step, once the deal price is submitted by both seller and buyer
        ;; funds will be transferred to contract owner and seller
        (transfer token-id (as-contract tx-sender) tx-sender)
    )
)

(define-public (transfer (token-id uint) (caller principal) (buyer principal))
    (let
        (
            (cost (default-to u1 (map-get? token_cost_mapping token-id)))
            (dealProfit (/ (* cost u5) u100))
            (owner (unwrap-panic (get-owner token-id)))
        )
        ;;following line will make sure the function is called from contract
        (asserts! (is-eq caller (as-contract tx-sender)) ERR_NO_OWNER)
        (try! (nft-transfer? tera_coin token-id (unwrap-panic owner) buyer))
        ;; ;; update the token owner and selling status in mapping
        (map-set token_status_mapping token-id false)
        (map-delete token_buyer_mapping token-id)
        (map-delete bid-mapping token-id)
        ;; transfer the contract owner and seller cost 
        (try! (as-contract (stx-transfer? dealProfit tx-sender CONTRACT_OWNER)))
        (try! (as-contract (stx-transfer? cost tx-sender (unwrap-panic owner))))
        (ok true)
    )
)

(define-read-only (get-token-cost (token-id uint))
    (ok (map-get? token_cost_mapping token-id))
)

(define-read-only (get-token-uri (token_id uint))
    (ok (some "https://www.tintash.com/"))
)

(define-read-only (get-last-token-id) 
    (ok (var-get last-minted-token))
)

(define-read-only (get-owner (token-id uint))
    (ok (nft-get-owner? tera_coin token-id))
)