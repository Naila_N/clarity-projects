import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

import { fbDataModel } from './fbData';

const firebaseConfig = {
  // configurations goes here
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Get a reference to the database service
export const db = firebase.firestore();
export function saveToken(id, cost, owner) {
  try {
    db.collection("minted_coins").doc(id.toString()).set({cost: cost, owner: owner});
  } catch (e) {
    console.error("Error ", e);
  }
}

export async function getData() {
  const data = await db.collection("minted_coins").get();
  var listOfData=[];
  data.docs.forEach(async (item) => {
    var fbData = new fbDataModel(item.id, item.get("cost"), item.get("owner"));
    listOfData.push(fbData);
  });
  return listOfData;
}

export default db;