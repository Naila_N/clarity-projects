//contract details
export const contractAddress = "ST1H4SF8Z6KA2KZGD2W3VAJH6STG4XDAK4KD36WBB";
export const contractName = "app-contract1";
// function names
export const mintNFTFunction = "mintNFT";
export const sellNFTFunction = "sellNFT";
export const bidForNFTFunction = "bidForNFT";
export const respondToBidFunction = "respondToBid";
export const submitDealPriceFunction = "submitDealPrice";
export const getLastMintedToken = "get-last-token-id";
export const getTokenCost = "get-token-cost";
export const getOwner = "get-owner";

