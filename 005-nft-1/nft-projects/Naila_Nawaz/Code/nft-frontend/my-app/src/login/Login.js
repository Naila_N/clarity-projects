import React, { useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
import { AppConfig, UserSession, showConnect } from '@stacks/connect';

import logo from '../logo.svg';
import './Login.css';
import { getData } from "../utils/firebase";

const appConfig = new AppConfig(['store_write', 'publish_data']);
const userSession = new UserSession({ appConfig });

function Login() {
    const history = useHistory();
    const [testnet, setTestnet] = useState("")
    const [mainnet, setMainnet] = useState("");
    var [fbData, setfbData] = useState("");

    const handleLogin = () => {
        showConnect({
        appDetails: {
            name: "Tera Coins",
            icon: window.location.origin + logo,
        },
        onFinish: () => {
            redirectToHome();
        },
        userSession: userSession,
        });
    };

    const redirectToHome = async() => {
        const userData = userSession.loadUserData();
        fbData = await getData();
        const profile = userData.profile.stxAddress;
        setTestnet(profile.testnet);
        setMainnet(profile.mainnet);
        setfbData(fbData); 
    }

    useEffect(() => {
        if (testnet && mainnet) {
            history.push('/home', { testnet: testnet, mainnet: mainnet, fbData, fbData } );
        }
      }, [testnet, mainnet, history]);

    return(
        <div className="Login">
            <div className="login-header">
                Welcome to Tera Coins. Connect your Hiro Wallet to Procede.
            </div>
            <button className="login-btn" onClick={handleLogin} >
                Connect Wallet
            </button>
        </div>
    ) 
}

export default Login;
