import { openContractCall } from '@stacks/connect';
import { trueCV, falseCV, uintCV,callReadOnlyFunction, cvToValue } from '@stacks/transactions';
import { StacksTestnet } from "@stacks/network";
import React, { useState } from "react";

import './Home.css';
import * as constants from '../utils/constants'
import { saveToken, getData } from '../utils/firebase';

function Home(props) {
    // Contract integration
    var fbData = props.fbData;
    const testnet = new StacksTestnet();
    const wallet = props.location.state;
    // create NFT
    const [nftCost, setNFTCost] = useState("");
    const [nftTxId, setNFTTxId] = useState("");
    const [showNFTSuccess, setNFTSuccess] = useState("");

    // sell NFT
    const [nftSellId, setNFTSellId] = useState("");
    const [sellTxId, setSellTxId] = useState("");
    const [showSellSuccess, setShowSellSuccess] = useState("");

    //bid For NFT
    const [nftBidId, setNFTBidId] = useState("");
    const [bidTxId, setBidTxId] = useState("");
    const [showBidSuccess, setShowBidSuccess] = useState("");

    //respond to bid
    const [nftBidResId, setNFTBidResId] = useState("");
    const [bidResTxId, setBidResTxId] = useState("");
    const [showBidResSuccess, setShowBidResSuccess] = useState("");
    const [nftBidRes, setNFTBidRes] = useState("");

    //submit deal price
    const [nftDealTokenId, setNFTDealToken] = useState("");
    const [dealtxId, setDealTxId] = useState("");
    const [showDealSuccess, setShowDealSuccess] = useState("");

    // Read only functions from contract
    const lastMintedToken = async() => {
        const options = createReadOnlyOptions(constants.getLastMintedToken, []);
        const result = await callReadOnlyFunction(options);
        const response = cvToValue(result);
        return response.value;
    }

    const getTokenCost = async(tokenId) => {
        const options = createReadOnlyOptions(constants.getTokenCost , [uintCV(tokenId)]);
        const result = await callReadOnlyFunction(options);
        const response = cvToValue (result);
        return response.value.value;
    }

    const getOwner = async(tokenId) => {
        const options = createReadOnlyOptions(constants.getOwner , [uintCV(tokenId)]);
        const result = await callReadOnlyFunction(options);
        const response = cvToValue (result);
        return response.value.value;
    }

    const refresh = async() => {
        // fetch cost for all the tokens with id <= last minted token and id > saved in db
        fbData = await getData();       
        var lastSavedIdInFb = fbData.size;
        var lastMintedTokenId = await lastMintedToken();
        // if 3 tokens are saved in db, and 7 tokens are minted, then fetch cost for token id
        // 4 to 7
        for (var i = lastSavedIdInFb + 1; i <= lastMintedTokenId; i++) {
            // call contract to find cost and save in db
            var owner = await getOwner(i);
            var cost = await getTokenCost(i);
            saveToken(i, cost, owner);
        } 
    }
    
    // NFT creation
    const createNFT = async() => {
        var opt = createOptions(constants.mintNFTFunction, [uintCV(nftCost)]);
        await openContractCall(opt);
    }

    const recordNFTCost = (text) => {
        setNFTCost(text.target.value);
    }

    // sellNFT
    const publishForSell = async() => {
        var opt = createOptions(constants.sellNFTFunction, [uintCV(nftSellId)]);
        await openContractCall(opt);
    }

    const recordNFTSellTokenId = (text) => {
        setNFTSellId(text.target.value);
    }

    // bidNFT
    const bidForNFT = async() => {
        var opt = createOptions(constants.bidForNFTFunction, [uintCV(nftBidId)]);
        await openContractCall(opt);
    }

    const recordNFTBidTokenId = (text) => {
        setNFTBidId(text.target.value);
    }

    // respondToBid
    const respondToBid = async() => {
        var opt;
        if (nftBidRes === "0")
            opt = createOptions(constants.respondToBidFunction, [falseCV(), uintCV(nftBidResId)]);
        else
            opt = createOptions(constants.respondToBidFunction, [trueCV(), uintCV(nftBidResId)]);
        await openContractCall(opt);
    }

    const recordBidResTokenId = (text) => {
        setNFTBidResId(text.target.value);
    }

    const recordBidSellerRes = (text) => {
        setNFTBidRes(text.target.value);
    }

     // submitDealPrice
     const submitDealPrice = async() => {
        var opt = createOptions(constants.submitDealPriceFunction, [uintCV(nftDealTokenId)]);
        await openContractCall(opt);
    }

    const recordDealTokenId = (text) => {
        setNFTDealToken(text.target.value);
    }

    
    const recordNFTTx = (funcName, txId) => {
        switch (funcName) {
            case constants.mintNFTFunction:
                setNFTSuccess(true);
                setNFTTxId(txId)
                break;

            case constants.sellNFTFunction:
                setSellTxId(txId);
                setShowSellSuccess(true);
                break;
            
            case constants.bidForNFTFunction:
                setBidTxId(txId);
                setShowBidSuccess(true);
                break;
            
            case constants.respondToBidFunction:
                setBidResTxId(txId);
                setShowBidResSuccess(true);
                break;
            case constants.submitDealPriceFunction:
                setDealTxId(txId);
                setShowDealSuccess(true);
                break;
            default:
                return;
        }
        
    }

    const createOptions = (fncName, fncArgs) => {
        return {
            contractAddress: constants.contractAddress,
            contractName: constants.contractName,
            functionName: fncName,
            functionArgs: fncArgs,
            appDetails: {
            name: 'My App',
            icon: window.location.origin + '/my-app-logo.svg',
            },
            network:testnet,
            onFinish: data => {
                console.log('Stacks Transaction:', data.stacksTransaction);
                console.log('Transaction ID:', data.txId);
                console.log('Raw transaction:', data.txRaw);
                recordNFTTx (fncName, data.txId)
            },
        }
    }

    const createReadOnlyOptions = (fncName, fncArgs) => {
        return {
            contractAddress: constants.contractAddress,
            contractName: constants.contractName,
            functionName: fncName,
            functionArgs: fncArgs,
            network: testnet,
            senderAddress: wallet.testnet,
        };
    }
      
    return (
        <div className="Home">
            <div className="header"> 
                Welcome to Tera Coins. 
                <button className="refresh" onClick={refresh}>Refresh</button>
            </div>
            
            <br/>
            <br/>
            <br/>

            <div>
                <div>
                    TestNet: {wallet.testnet}
                </div>
                <b/>
                <div>
                    Mainnet: {wallet.mainnet}
                </div>
            </div>
            
            <div className="Title">
                TERA COINS 
            </div>
            
          
            <div>
                <label className="nft-label">Enter the cost if you want to create a Tera Coin</label> <br/>
                <div className="nft-div">
                    <input
                        className="nft-input"
                        name="nftCost"
                        type="number"
                        min="20"
                        placeholder="Enter cost"
                        onChange={recordNFTCost} />

                    <button className="nft-btn" onClick={createNFT}>Create Tera Coin</button>

                    {showNFTSuccess && (
                        <div className="nft-div">
                            <input className="nft-tx"
                                name="txCreateNFT"
                                type="text"
                                value={nftTxId}
                                onChange={recordNFTTx}
                                placeholder="Transaction id will appear here"
                                />
                            
                            <div className="nft-success-msg">
                                <label className='nft-success-msg'>Tera Coin Created Successfully</label>
                            </div>
                        </div>
                    )}
                </div>
            </div>

            <div>
                <br/>
                <br/>
                <label className="nft-label">Enter the coin id if you want to publish it for sell</label> <br/>
                <div className="nft-div">
                    <input
                        className="nft-input"
                        name="nftSell"
                        placeholder="Enter Token Id"
                        onChange={recordNFTSellTokenId} />

                    <button className="nft-btn" onClick={publishForSell}>Publish Tera Coin for sell</button>

                    {showSellSuccess && (
                        <div className="nft-div">
                            <input className="nft-tx"
                                name="txSellNFT"
                                type="text"
                                value={sellTxId}
                                onChange={recordNFTTx}
                                placeholder="Transaction id will appear here"
                                />
                            
                            <div className="nft-success-msg">
                                <label className='nft-success-msg'>Tera Coin Published Successfully</label>
                            </div>
                        </div>
                    )}
                    
                </div>
            </div>
        
            <div>
            <br/>
                <br/>
                <label className="nft-label">Enter the coin id if you want to bid for a coin</label> <br/>
                <div className="nft-div">
                    <input
                        className="nft-input"
                        name="nftSell"
                        placeholder="Enter Token Id"
                        onChange={recordNFTBidTokenId} />

                    <button className="nft-btn" onClick={bidForNFT}>Bid for Tera Coin</button>

                    {showBidSuccess && (
                        <div className="nft-div">
                            <input className="nft-tx"
                                name="txBidNFT"
                                type="text"
                                value={bidTxId}
                                onChange={recordNFTTx}
                                placeholder="Transaction id will appear here"
                                />
                            
                            <div className="nft-success-msg">
                                <label className='nft-success-msg'>Tera Coin Sent Successfully</label>
                            </div>
                        </div>
                    )}
                    
                </div>
            </div>

            <div>
            <br/>
                <br/>
                <label className="nft-label">Enter the coin id if you want to accept/reject the bid, along with acceptance (1) or rejection (0) response</label> <br/>
                <div className="nft-div">
                    <input
                        className="nft-input"
                        name="nftSell"
                        placeholder="Enter Token Id"
                        onChange={recordBidResTokenId} />
                    
                    <input
                        className="nft-input"
                        type="number"
                        min="0"
                        max="1"
                        placeholder="Enter Bid Response - 1 (accept) / 0 (reject)"
                        onChange={recordBidSellerRes} />

                    <button className="nft-btn" onClick={respondToBid}>Respond to Bid</button>

                    {showBidResSuccess && (
                        <div className="nft-div">
                            <input className="nft-tx"
                                name="txBidNFT"
                                type="text"
                                value={bidResTxId}
                                onChange={recordNFTTx}
                                placeholder="NFT transaction id will appear here"
                                />
                            
                            <div className="nft-success-msg">
                                <label className='nft-success-msg'>Bid Responded Successfully</label>
                            </div>
                        </div>
                    )}
                    
                </div>
            </div>

            <br/>
                <br/>
                <label className="nft-label">Enter the coin id if you want to submit deal price for it</label> <br/>
            <div>
                <div className="nft-div">
                    <input
                        className="nft-input"
                        name="nftSell"
                        placeholder="Enter Token Id"
                        onChange={recordDealTokenId} />

                    <button className="nft-btn" onClick={submitDealPrice}>Submit Deal Price</button>

                    {showDealSuccess && (
                        <div className="nft-div">
                            <input className="nft-tx"
                                name="txBidNFT"
                                type="text"
                                value={dealtxId}
                                onChange={recordNFTTx}
                                placeholder="NFT transaction id will appear here"
                                />
                            
                            <div className="nft-success-msg">
                                <label className='nft-success-msg'>Bid Price Submitted Successfully</label>
                            </div>
                        </div>
                    )}
                    
                </div>
            </div>
            <br/> <br/>
        </div>
    );
}

export default Home;
