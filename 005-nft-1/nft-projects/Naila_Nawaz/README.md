# REAL ESTATE

## Problem Statement
In real estate, the owner of property holds a certificate which shows his ownership. If he wants to transfer this property's ownership to anyone, he'll transfer the ownership of that certificate. Certificates in this case are NFTs. 

## Minting Process
* Propoerty owner/ Seller creates an NFT for his property by calling a function in contract. This function will generate a 'Tera coin' against that property.  (This tera coin is basically the certificate of that property)

## Application Flow
* Seller will change the status of this 'Tera coin' to 'sale' when he wish to sell his property, along with the price he wish to charge.
* Buyer will send him an offer if he wish to purchase that property.
* Once seller and buyer agree upon a price, they both will send 5% of the deal price to contract (as the contract usage charges),  and the deal price will also be submitted to the contract.
* Seller will call a contract function to change the ownership of 'Tera coin'. 
* Contract will change the ownership of 'Tera coin' from buyer to seller. 5% deal price will be sent to Contract Owner , and deal price will be sent to seller.
