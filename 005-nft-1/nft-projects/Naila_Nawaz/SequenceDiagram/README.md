# REAL ESTATE
Problem Statement
In real estate, the owner of property holds a certificate which shows his ownership. If he wants to transfer this property's ownership to anyone, he'll transfer the ownership of that certificate. Certificates in this case are NFTs.

## Minting Process
Propoerty owner/ Seller creates an NFT for his property by calling a function in contract. This function will generate a 'Tera coin' against that property. (This tera coin is basically the certificate of that property)

## Application Flow
Property owner will create a 'Tera coin' using MintNFT function
NFT cost will be >= 20

If owner wants to sell the NFT, he'll change the selling status of Tera coin to true (By default it will be false) by using sellNFT function. Sell NFT will make sure the function is being called by the owner of coin.

If buyer wants to purchase some NFT, he'll bid for it, by using BidNFT function. The function will make sure that function is not called by the owner. The token for which buyer wants to bid is up for selling. There'll be only one person bidding at one time, function will make sure that no one else is bidding for this tera coin. This will be maintianed in a bidding map

If seller wants to accept the bid by this seller, he'll accept it by submitting 5% of tera coin cost. Bid status will be changed to accepted. The function will make sure that it's being called by the owner of coin and he has 5% cost in his wallet. Also, that some one has bid on this coin.

Buyer will submit the cost of coin along with his 5% fee. The function will check that buyer who has bid on this coin is calling the function and the seller has responded to this bid. 

Contract will transfer the ownership of coin to buyer. 10% transfer fee will be sent to contract owner, and the cost of Tera coin will be transferred to seller. Also, it'll make sure that the function is being called by contract only. Tera coin selling and bid statuses will be changed to false.

If the seller has accepted the bid and buyer doesn't submit the cost, seller can claim funds reversal after 20 blocks. The function will make sure that 20 blocks have passed and the function is being called by seller only.

If the buyer want to back off from the bid, 5% amount will be sent back to seller if he has responded to the bid.